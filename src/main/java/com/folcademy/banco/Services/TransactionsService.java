package com.folcademy.banco.Services;

import com.folcademy.banco.models.dto.*;
import com.folcademy.banco.models.entities.AccountEntity;
import com.folcademy.banco.models.entities.TransactionsEntity;
import com.folcademy.banco.models.entities.UserEntity;
import com.folcademy.banco.models.mappers.TransactionMapper;
import com.folcademy.banco.models.repositories.AccountRepositories;
import com.folcademy.banco.models.repositories.TransactionsRepositories;
import com.folcademy.banco.models.repositories.UserRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionsService {
    private final TransactionsRepositories transactionsRepositories;
    private  final AccountRepositories accountRepositories;
    private  final UserRepositories userRepositories;
    private  final TransactionMapper transactionMapper;

    public TransactionsService(TransactionsRepositories transactionsRepositories, AccountRepositories accountRepositories, UserRepositories userRepositories, TransactionMapper transactionMapper) {
        this.transactionsRepositories = transactionsRepositories;
        this.accountRepositories = accountRepositories;
        this.userRepositories = userRepositories;
        this.transactionMapper = transactionMapper;
    }
    public TransactionsDTO getTransactions(Long accountNumber){
        List<TransactionsEntity> transactionsEntities= transactionsRepositories.findAllByOriginOrDestination(accountNumber.toString(),accountNumber.toString());
        List<TransactionDTO> transactionDTOList = new ArrayList<>();

        for (TransactionsEntity entity: transactionsEntities){
            AccountEntity toAccountsEntity = accountRepositories.findByNumber(Long.parseLong(entity.getDestination()));
            UserEntity toUserEntity = userRepositories.findByDni(toAccountsEntity.getUserId());
            String typeTransaction;

            if(entity.getOrigin().equals(accountNumber.toString())){
                typeTransaction = "GASTO";
            }else{
                typeTransaction="INGRESO";
            }

            transactionDTOList.add(

                    new TransactionDTO(
                            entity.getDate().toString(),
                            entity.getDescription(),
                            entity.getAmount(),
                            entity.getCurrency().toString(),
                            entity.getOrigin(),
                            toUserEntity.fullName()+"/ CBU "+toAccountsEntity.getCbu(),
                            typeTransaction)
                    );
            };

        BigDecimal balance = BigDecimal.ZERO;
        for (TransactionDTO transactionDTO:transactionDTOList){
                if(transactionDTO.getType().equals("GASTO")){
                        balance = balance.subtract(transactionDTO.getAmount());
                }else{
                        balance = balance.add(transactionDTO.getAmount());
                }
        }
        return new TransactionsDTO(transactionDTOList,balance);
    }
    public ResponseEntity<String> createTransaction(NewTransactionDTO newTransactionDTO){
        try {

            if(newTransactionDTO.getFrom()!=newTransactionDTO.getTo()){
                AccountEntity accountEntity =   accountRepositories.findById(Long.parseLong(newTransactionDTO.getTo())).get();
                if(accountEntity!=null){
                    BigDecimal cero = new BigDecimal(0);
                    if(newTransactionDTO.getAmount().compareTo(cero)==1){
                        TransactionsEntity transactionsEntity = transactionMapper.mapTransactionDtoToTransactionEntity(newTransactionDTO);
                        transactionsRepositories.save(transactionsEntity);
                        return new ResponseEntity<>("Transaccion creada", HttpStatus.OK);
                    }else{
                        return new ResponseEntity<>("Error al crear la transacción", HttpStatus.NOT_FOUND);
                    }

                }else{
                    return new ResponseEntity<>("Error al crear la transacción", HttpStatus.NOT_FOUND);
                }

            }else{
                return new ResponseEntity<>("Error al crear la transacción", HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            return new ResponseEntity<>("Algo salió mal", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public DetailedTransactionDTO getTransaction(Long transactionId){

        TransactionsEntity transactionsEntities= transactionsRepositories.findById(transactionId).get();
        AccountEntity toAccountsEntityDetination = accountRepositories.findByNumber(Long.parseLong(transactionsEntities.getDestination()));
        AccountEntity toAccountsEntityOrigin = accountRepositories.findByNumber(Long.parseLong(transactionsEntities.getOrigin()));
        UserEntity toUserEntityOrigin = userRepositories.findByDni(toAccountsEntityOrigin.getUserId());
        UserEntity toUserEntityDetination = userRepositories.findByDni(toAccountsEntityDetination.getUserId());

        return new DetailedTransactionDTO(
                transactionsEntities.getDescription(),
                transactionsEntities.getCurrency().toString(),
                transactionsEntities.getAmount(),
                new UserAccountDTO(toAccountsEntityOrigin.getCbu(),toUserEntityOrigin.getName(),toUserEntityOrigin.getLast_name()),
                new UserAccountDTO(toAccountsEntityDetination.getCbu(),toUserEntityDetination.getName(),toUserEntityDetination.getLast_name()),
                transactionsEntities.getDate().toString());
    }
}
