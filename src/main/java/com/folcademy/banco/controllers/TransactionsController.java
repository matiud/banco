package com.folcademy.banco.controllers;

import com.folcademy.banco.Services.TransactionsService;
import com.folcademy.banco.models.dto.DetailedTransactionDTO;
import com.folcademy.banco.models.dto.NewTransactionDTO;
import com.folcademy.banco.models.dto.TransactionsDTO;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transactions")
public class TransactionsController {

    private  final TransactionsService  transactionsService;

    public TransactionsController(TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }


    @GetMapping("/all/{accountNumber}")
    public ResponseEntity<TransactionsDTO> getTransactionsForAccount(@PathVariable Long accountNumber){
        return new ResponseEntity<>(transactionsService.getTransactions(accountNumber), HttpStatus.OK);
    }
    @PostMapping("/new")
    public  ResponseEntity<String> createTransaction(@RequestBody NewTransactionDTO newTransactionDTO){
            return transactionsService.createTransaction(newTransactionDTO);
    }

    @GetMapping("/{transactionId}")
    public ResponseEntity<DetailedTransactionDTO> getTransaction(@PathVariable Long transactionId){
        return  new ResponseEntity<>(transactionsService.getTransaction(transactionId),HttpStatus.OK);
    }
}
