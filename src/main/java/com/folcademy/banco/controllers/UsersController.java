package com.folcademy.banco.controllers;

import com.folcademy.banco.models.entities.UserEntity;
import com.folcademy.banco.models.repositories.UserRepositories;
import org.springframework.web.bind.annotation.*;

@RestController

@RequestMapping("/users")
public class UsersController {
    private final UserRepositories userRepository;

    public UsersController(UserRepositories userRepositories) {
        this.userRepository = userRepositories;
    }

    @PostMapping("/new")
    public UserEntity newUser(@RequestBody UserEntity userEntity){
        return  userRepository.save(userEntity);
    }


}
