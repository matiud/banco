package com.folcademy.banco.controllers;

import com.folcademy.banco.models.entities.AccountEntity;
import com.folcademy.banco.models.repositories.AccountRepositories;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountsController {
    private final AccountRepositories accountRepository;

    public AccountsController(AccountRepositories accountRepository) {
        this.accountRepository = accountRepository;
    }

    @GetMapping("/{userId}")
    public List<AccountEntity> getUserAccounts(@PathVariable String userId){
        return accountRepository.findAllByUserId(userId);
    }
    @PostMapping("/new")
    public AccountEntity newAccount(@RequestBody AccountEntity accountEntity){
        return  accountRepository.save(accountEntity);
    }
}
