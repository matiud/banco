package com.folcademy.banco.models.repositories;

import com.folcademy.banco.models.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserRepositories extends CrudRepository<UserEntity,String> {
    UserEntity findByDni(String dni);
}
