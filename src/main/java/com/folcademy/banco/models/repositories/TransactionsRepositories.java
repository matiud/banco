package com.folcademy.banco.models.repositories;

import com.folcademy.banco.models.entities.TransactionsEntity;
import com.folcademy.banco.models.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionsRepositories extends CrudRepository<TransactionsEntity,Long> {
    List<TransactionsEntity> findAllByOriginOrDestination(String origin,String destination);

}
