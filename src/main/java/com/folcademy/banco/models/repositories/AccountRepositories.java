package com.folcademy.banco.models.repositories;

import com.folcademy.banco.models.entities.AccountEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountRepositories extends CrudRepository<AccountEntity,Long> {
    List<AccountEntity> findAllByUserId(String userId);
    AccountEntity findByNumber(long number);
}
