package com.folcademy.banco.models.dto;



import java.math.BigDecimal;

public class DetailedTransactionDTO {

    private  String description;
    private String currency;
    private BigDecimal amount;
    private UserAccountDTO from;
    private UserAccountDTO to;
    private String date;

    public DetailedTransactionDTO() {
    }

    public DetailedTransactionDTO(String description, String currency, BigDecimal amount, UserAccountDTO from, UserAccountDTO to, String date) {
        this.description = description;
        this.currency = currency;
        this.amount = amount;
        this.from = from;
        this.to = to;
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public UserAccountDTO getFrom() {
        return from;
    }

    public UserAccountDTO getTo() {
        return to;
    }

    public String getDate() {
        return date;
    }
}
