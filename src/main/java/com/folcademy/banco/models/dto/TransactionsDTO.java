package com.folcademy.banco.models.dto;
import java.math.BigDecimal;
import java.util.List;

public class TransactionsDTO {
    private List<TransactionDTO> transactions;
    private BigDecimal balance;

    public TransactionsDTO() {
    }


    public TransactionsDTO(List<TransactionDTO> transactions, BigDecimal balance) {
        this.transactions = transactions;
        this.balance = balance;
    }
    public TransactionsDTO(List<TransactionDTO> transactions) {
        this.transactions = transactions;
    }
    public List<TransactionDTO> getTransactions() {
        return transactions;
    }
    public  int getTransactionNumber(){
        return transactions.size();
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
