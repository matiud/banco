package com.folcademy.banco.models.dto;

import java.math.BigDecimal;

public class TransactionDTO {

    private String date;
    private  String description;
    private BigDecimal amount;
    private String currency;
    private String from;
    private String to;
    private String Type;

    public TransactionDTO() {
    }

    public TransactionDTO(String date, String description, BigDecimal amount, String currency, String origin, String destination, String type) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = origin;
        this.to = destination;
        Type = type;
    }



    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getType() {
        return Type;
    }
}
