package com.folcademy.banco.models.dto;

import com.folcademy.banco.models.enums.AccountsType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class UserAccountDTO {
    private String cbu;
    private String firstName;
    private String lastName;


    public UserAccountDTO() {
    }

    public UserAccountDTO(String cbu, String firstName, String lastName) {
        this.cbu = cbu;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getCbu() {
        return cbu;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
