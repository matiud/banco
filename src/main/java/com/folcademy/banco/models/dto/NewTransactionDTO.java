package com.folcademy.banco.models.dto;

import java.math.BigDecimal;
import java.util.Currency;

public class NewTransactionDTO {
    private  String description;
    private BigDecimal amount;
    private Currency currency;
    private String from;
    private String to;

    public NewTransactionDTO() {
    }

    public NewTransactionDTO(String description, BigDecimal amount, Currency currency, String origin, String destination) {
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = origin;
        this.to = destination;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }
}
