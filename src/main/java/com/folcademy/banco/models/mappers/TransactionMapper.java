package com.folcademy.banco.models.mappers;

import com.folcademy.banco.models.dto.NewTransactionDTO;
import com.folcademy.banco.models.entities.TransactionsEntity;
import org.springframework.stereotype.Component;

import java.util.Date;
@Component
public class TransactionMapper {
    public TransactionsEntity mapTransactionDtoToTransactionEntity(NewTransactionDTO newTransactionDTO){
            return  new TransactionsEntity(new Date(),newTransactionDTO.getDescription(),newTransactionDTO.getAmount(),newTransactionDTO.getCurrency(), newTransactionDTO.getFrom(), newTransactionDTO.getTo());
    }

}
