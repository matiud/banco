package com.folcademy.banco.models.entities;



import javax.persistence.*;


@Entity(name="users")
public class UserEntity {
    @Id
    private String dni;
    private String name;
    private String lastName;
    private String address;

    public UserEntity() {
    }

    public UserEntity(String dni, String name, String lastName, String address) {
        this.dni = dni;
        this.name = name;
        this.lastName = lastName;
        this.address = address;
    }

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getLast_name() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String fullName(){
        return  name+" "+lastName;
    }

}
