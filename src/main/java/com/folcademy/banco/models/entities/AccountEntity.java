package com.folcademy.banco.models.entities;

import com.folcademy.banco.models.enums.AccountsType;

import javax.persistence.*;

@Entity(name="accounts")
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  long id;
    private long number;
    private String cbu;
    @Enumerated(EnumType.STRING)
    private AccountsType type;
    private String userId;

    public AccountEntity() {
    }

    public AccountEntity(long id, long number, String cbu, AccountsType type, String userId) {
        this.id = id;
        this.number = number;
        this.cbu = cbu;
        this.type = type;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public long getNumber() {
        return number;
    }

    public String getCbu() {
        return cbu;
    }

    public AccountsType getType() {
        return type;
    }

    public String getUserId() {
        return userId;
    }
}
